CREATE TABLE `bd_multas`.`tbl_vehiculo` ( `patente` VARCHAR(6) NOT NULL , `tipo_vehiculo` VARCHAR(200) NOT NULL , PRIMARY KEY (`patente`)) ENGINE = InnoDB;
CREATE TABLE `bd_multas`.`tbl_permisocirculacion` ( `id` INT NOT NULL AUTO_INCREMENT ,  `patente` VARCHAR(6) NOT NULL ,  `valor_permiso` INT NOT NULL ,  `interes_reajuste` INT NOT NULL ,    PRIMARY KEY  (`id`),    UNIQUE  (`patente`)) ENGINE = InnoDB;
CREATE TABLE `bd_multas`.`tbl_multas` ( `id` INT NOT NULL AUTO_INCREMENT , `patente` VARCHAR(6) NOT NULL , `valor_multa` INT NOT NULL , PRIMARY KEY (`id`), UNIQUE (`patente`)) ENGINE = InnoDB;

ALTER TABLE `tbl_multas` ADD CONSTRAINT `vehiculo-multa` FOREIGN KEY (`patente`) REFERENCES `tbl_vehiculo`(`patente`) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE `tbl_permisocirculacion` ADD CONSTRAINT `vehiculo-permiso` FOREIGN KEY (`patente`) REFERENCES `tbl_vehiculo`(`patente`) ON DELETE CASCADE ON UPDATE CASCADE; 


select tv.patente, tv.tipo_vehiculo, tp.valor_permiso, tp.interes_reajuste, tm.valor_multa FROM tbl_vehiculo as tv, tbl_permisocirculacion as tp, tbl_multas as tm WHERE tv.patente = 'CKXX38' and tv.patente = tm.patente AND tv.patente = tp.patente and tp.id = tm.id;

INSERT INTO tbl_vehiculo AS tv, tbl_permisocirculacion as tp, tbl_multas as tm(tv.patente,tv.tipo, tp.patente, tp.valor_permiso, tp.interes_reajuste, tm.patente, tm.valor) VALUES ('BZXJ51', 'NISSAN TERRANO DX 4X2', 'BZXJ51', 51996, 0,'BZXJ51', 0);

INSERT INTO tbl_vehiculo(patente, tipo_vehiculo) VALUES ('BZXJ51','NISSAN TERRANO DX 4X2');
INSERT INTO tbl_permisocirculacion(patente,valor_permiso,interes_reajuste) VALUES ('BZXJ51',51996, 0 );
INSERT INTO tbl_multas(patente, valor_multa) VALUES('BZXJ51',0 );

INSERT INTO tbl_vehiculo(patente, tipo_vehiculo) VALUES ('CJCT94','NISSAN TERRANO DXS MT');
INSERT INTO tbl_permisocirculacion(patente,valor_permiso,interes_reajuste) VALUES ('CJCT94',60250, 4972 );
INSERT INTO tbl_multas(patente, valor_multa) VALUES('CJCT94',54164 );



