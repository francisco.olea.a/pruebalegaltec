<?php

include_once 'multa.php';



class ApiMultas{


    function getAll(){
        $multa = new Multa();
        $multas = array();
        $multas["vehiculos"] = array();

        $res = $multa->obtenerMulta();

        if($res->rowCount()){
            while($row = $res->fetch(PDO::FETCH_ASSOC)){
                $item = array(
                    'patente' => $row['patente'],
                    'tipo' => $row['tipo']
                );
                array_push($multas['vehiculos'], $item);
            }

            echo json_encode($multas);

        }else{
            echo json_encode(array('mensaje' => 'No hay elementos registrados'));
        }
    }

    function getByPatente($patente){
        $multa = new Multa();
        $multas = array();
        $multas["vehiculos"] = array();
        
        $res = $multa->obtenerMultaVehiculo($patente);

        if($res->rowCount() == 1){
            $row = $res->fetch(PDO::FETCH_ASSOC); 
                    
            $item=array(
                "patente" => $row['patente'],
                "tipo_vehiculo" => $row['tipo_vehiculo'],
                "valor_permiso" => $row['valor_permiso'],
                "interes_reajuste" => $row['interes_reajuste'],
                "valor_multa" => $row['valor_multa']
            ); 
            array_push($multas["vehiculos"], $item);
            //echo json_encode($multas);
            //$this->printJSON($multas);
            //return $row;
              
            return json_encode($multas, true);
        }else{
            echo json_encode(array('mensaje' => 'No hay elementos'));
        }
    }

    function addMulta($item){
        
        $multa = new Multa();
        $multas = array();
        $multas["vehiculos"] = array();
        
        $res = $multa->nuevaMulta($item);

        if($res->rowCount() == 1){
            $row = $res->fetch(PDO::FETCH_ASSOC); 
                    
            $item=array(
                "patente" => $row['patente'],
                "tipo" => $row['tipo'],
                "valor_permiso" => $row['valor_permiso'],
                "interes_reajuste" => $row['interes_reajuste'],
                "valor_multa" => $row['valor_multa']
                
            ); 
            array_push($multas["vehiculos"], $item);
            //echo json_encode($multas);
            //$this->printJSON($multas);
            //return $row;
              
            return json_encode($multas, true);
        }else{
            echo json_encode(array('mensaje' => 'No hay elementos'));
        }
    }

    function error($mensaje){
        echo '<code>' . json_encode(array('mensaje' => $mensaje)) . '</code>'; 
    }

    function exito($mensaje){
        echo '<code>' . json_encode(array('mensaje' => $mensaje)) . '</code>'; 
    }

    function printJSON($array){
        echo '<code>'.json_encode($array).'</code>';
    }

    

}

?>