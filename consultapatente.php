<html lang="es">
<header>
    <meta charset="UTF-8">
    <h1>Consulta Patente</h1>
</header>

<body>
    <form action="" method="GET">
        <label for="patente">Ingrese la patente a consultar</label>
        <input type="text" name="patente">
        <button type="submit">Buscar</button>
    </form>
    <button onclick="location.href='index.php'">Volver</button>

    <?php

    include_once "apimultas.php";
    $api = new ApiMultas();

    if (isset($_GET['patente'])) {

        $patente = $_GET['patente'];

        if (!is_int($patente)) {
            $res = $api->getByPatente($patente);
            $multas = json_decode($res, true);
           
            if(isset($multas) ){
                echo "
                <table border = 1 cellspacing = 1 cellpadding = 1>
                        <tr>
                        <th>Patente</th>
                        <th>Tipo</th>
                        <th>Valor Permiso Circulacion</th>
                        <th>Interes y Reajuste</th>
                        <th>valor Multa</th>
                        <th>Subtotal</th>
                        </tr>";
            
                echo "
                    <tr>
                        <td>" . $multas['vehiculos'][0]['patente']. "</td>
                        <td>" . $multas['vehiculos'][0]['tipo_vehiculo'] . "</td>
                        <td>" . $multas['vehiculos'][0]['valor_permiso'] . "</td>
                        <td>" . $multas['vehiculos'][0]['interes_reajuste'] . "</td>
                        <td>" . $multas['vehiculos'][0]['valor_multa'] . "</td>
                        <td>" . ($multas['vehiculos'][0]['valor_permiso'] + $multas['vehiculos'][0]['interes_reajuste'] + $multas['vehiculos'][0]['valor_multa']) . "</td>
                        </tr>";
            
            echo "</table>";  
            }     
        } else {
            $api->error('La patente es incorrecta');
        }
    } else {
        //$api->getAll();

    }

    ?>
</body>

</html>