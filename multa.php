<?php

include_once 'db.php';

class Multa extends DB
{

    function obtenerMulta()
    {
        //$pat = '.' . $patente .'.';
        //echo($pat);
        //$query = $this->connect()->query('select tv.patente, tv.tipo, tp.valor_permiso, tp.interes_reajuste, tm.valor FROM tbl_vehiculo as tv, tbl_permisocirculacion as tp, tbl_multas as tm WHERE tv.patente =' . $pat . ' and tp.patente = tm.patente');
        //echo($query);
        $query = $this->connect()->query('SELECT * FROM tbl_vehiculo');
        return $query;
    }

    function obtenerMultaVehiculo($patente)
    {
        $query = $this->connect()->prepare('SELECT tv.patente, tv.tipo_vehiculo, tp.valor_permiso, tp.interes_reajuste, tm.valor_multa FROM tbl_vehiculo as tv, tbl_permisocirculacion as tp, tbl_multas as tm WHERE tv.patente = :patente and tv.patente = tm.patente AND tv.patente = tp.patente and tp.id = tm.id');
        $query->execute(['patente' => $patente]);
        return $query;
    }

    function nuevaMulta($item)
    {
        $q1 = $this->connect()->prepare('INSERT INTO tbl_vehiculo(patente, tipo) VALUES (:patente, :tipo_vehiculo)');
        if ($q1 == true) {
            $q2 = $this->connect()->prepare('INSERT INTO tbl_permisocirculacion(patente,valor_permiso,interes_reajuste) VALUES (:patente, :valor_permiso, :interes_reajuste )');
            if ($q2 == true) {
                $q3 = $this->connect()->prepare('INSERT INTO tbl_multas(patente,valor_multa) VALUES (:patente, :valor_multa )');
                if ($q3 == true) {
                    $query = $this->connect()->prepare('SELECT tv.patente, tv.tipo_vehiculo, tp.valor_permiso, tp.interes_reajuste, tm.valor_multa FROM tbl_vehiculo as tv, tbl_permisocirculacion as tp, tbl_multas as tm WHERE tv.patente = :patente and tv.patente = tm.patente AND tv.patente = tp.patente and tp.id = tm.id');
                    $query->execute(['patente' => $item['patente'], 'tipo_vehiculo' => $item['tipo_vehiculo'], 'valor_permiso' => $item['valor_permiso'], 'interes_reajuste' => $item['interes_reajuste'], 'valor_multa' => $item['valor_multa']]);
                }
            }
        }

        return $query;
    }
}
